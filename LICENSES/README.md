# Licenses

`Data display` for LiberaForms is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE version 3 or later.

Third party libraries are listed in the NOTICE file.

Included in this directory are copies of formentioned licenses.
