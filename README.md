# Data display

A LiberaForms VueJS component developed with Vue3 and https://vue-loader.vuejs.org/

Data display primarily provides a responsive and reactive UI to display lists of data, like the user's forms, or form answers.

Also, E2EE Answers must be decrypted on the client. Data display is the place we do that.

Data display imports [openPGP.js](https://openpgpjs.org/) and provides:

* Key pair creation and management
* Key restore
* Answer decryption
* Editor answer edition

## The Panel

A control panel of data options corresponding to the list of data being displayed.

* Exports PDF, CSV, and JSON
* Search, Order by, Ascending.
* Filters multi option fields.
* Delete options.
* Column order. Move any column to the first column position.

### Grid

* Each row begins with 'control column' that includes a `bookmark` button for your convenience.
* And effort is made to set the grid's column widths to improve readability.

### Cards

Each data item is displayed as a card. The title of the card is the first data column. Click on the title to expand the card.

### Map

Available when a form includes a 'Map' field.

### Data inspection

Fields are clickable when it makes sense to:

* Use the `Copy` button to copy the field value to the clipboard
* Use the `Edit` button to edit the field value

### User preferences

These preferences are saved to the database and recovered when the user returns to read that same data.

* The last visualized rendering mode (grid/cards/graphs)
* Multi option field filters.
* PDF export options.
* Column order and ascending/descending.

### Exports

* **PDF**: Exports filtered items with various options. Font size, page size, etc.
* **JSON**: Exports filtered items in JSON
* **CSV**: Export filtered items in CSV

## Answer history

Answers can be edited when:

* The form's anonymous answer edition is enabled
* Editors make changes

Editors can compare the edited versions of each answer.

## Development

### Start the webpack development server

You will need to run:

```
$ npm install
$ npm run serve
```

Which will give you something like this:
```
$ npm run serve

> data-display@3.0.1 serve
> npm exec -- webpack-dev-server --mode=development --hot

<i> [webpack-dev-server] Project is running at:
<i> [webpack-dev-server] Loopback: http://localhost:8080/
<i> [webpack-dev-server] On Your Network (IPv4): http://192.168.[...]:8080/
<i> [webpack-dev-server] On Your Network (IPv6): http://[fe80::1]:8080/
<i> [webpack-dev-server] Content not from webpack is served from '[...]public' directory
[...]
```

Make sure you write down one of the `http` locations that are printed at the
beginning of the command.
That's where the webpack development server is listening, and where we will
instruct `liberaforms/server` to request the `liberaforms/data-display`
components, see next section.

### Configure `liberaforms/server`

For this, you will edit the `.env` file as you would for any other site-wide
setting and add or modify this variable:

```
# Editing: liberaforms/.env
# Make sure the value matches what "npm run serve" printed!
# Unless you are using a lot of development services, this might not change
DATA_DISPLAY_BASE_URL="http://localhost:8080"
```

If it was running, you will need to restart `liberaforms/server` for changes
to apply.

### Checking the backend

You may also wish to read the backend code (`liberaforms/server`) under:
`/liberaforms/views/data_display/`

### Checking that hot loading is working

When first started, `npm run serve` will print after several seconds
something ilke:

```
webpack 5.93.0 compiled with 3 warnings in 32439 ms
```

Now, if you modify, say `src/loaders/dataDisplayLoader.js` (adding a blank
line or `console.log("Ground Control to Major Tom")` should suffice),
the output should be along the lines of:

```
webpack 5.93.0 compiled successfully in 279 ms
```

That is: a reduction of about 2 orders of magnitude.

Note that the webpack web server will stall any requests as necessary while
rebuilding the components, so if you notice that the delay is incredibly long,
it might be worth it to check that hot loading is working!

## Funding

This project received funding through [NGI Zero PET](https://nlnet.nl/PET) and [NGI Assure](https://nlnet.nl/assure), funds established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project](https://nlnet.nl/project/Liberaforms) [pages](https://nlnet.nl/project/LiberaForms-E2EE).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGI0_tag.svg" alt="NGI Zero Logo" width="20%" />](https://nlnet.nl/PET)
[<img src="https://nlnet.nl/image/logos/NGIAssure_tag.svg" alt="NGI Assure Logo" width="20%" />](https://nlnet.nl/assure)
