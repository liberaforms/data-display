/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { inject } from 'vue';
import { keys,
         keyStorage,
         b64,
         deleteDeviceKeys } from "@/modules/e2ee-answers.js";

const prefix = 'LF_E2EE_Key_'
const e2ee_status = {}
const current_ui_key_id = {}
const public_key_on_server = {}  // user public_key
const form_key_on_server = {}  // form public_key
const form_key_id = {}
const editor_key_id = {}

export function loadInjections() {

  form_key_id.value = inject('e2ee_form_key_id', null);
  editor_key_id.value = inject('e2ee_editor_key_id');
  e2ee_status.value = JSON.parse(inject('e2ee_status'));

  public_key_on_server.value =
    // user key
    inject('public_key_on_server') !== "{}"
      ? JSON.parse(inject("public_key_on_server"))
      : undefined;

  if (e2ee_status.value.form_id !== undefined &&
      e2ee_status.value.is_e2ee_enabled !== undefined) {
    form_key_on_server.value = e2ee_status.value.is_e2ee_enabled
  }

  return {
    form_key_id: form_key_id.value,
    editor_key_id: editor_key_id.value,
    public_key_on_server: public_key_on_server.value,
    e2ee_status: e2ee_status.value,
  }
}

export async function digestDeviceStorage() {

  if (!editor_key_id.value)
    loadInjections() // ensure we have the injections

  /*
    user keys
  */
  let local_public_key;
  try {
    local_public_key = await keys.getPublic(
      keyStorage.readPublic(editor_key_id.value, false)
    );
  }
  catch {
    console.log('editor local_public_key invalid')
    await deleteDeviceKeys(editor_key_id.value)
    return await digestDeviceStorage()
  }
  let has_local_key = Boolean(local_public_key && local_public_key.fingerprint);

  let lockedKey = await keyStorage.readLocked(editor_key_id.value);
  if (lockedKey) {
    try {
      // persists key as unlocked to sessionStorage / when key doesn't have a passphrase
      await keys.getPrivate(lockedKey)
    }
    catch {
      console.log('editor keys.getPrivate(lockedKey) invalid')
      await deleteDeviceKeys(editor_key_id.value)
      return await digestDeviceStorage()
    }
  }
  let session_public_key;
  try {
    session_public_key = await keys.getPublic(
      keyStorage.readPublic(editor_key_id.value, true)
    );
  }
  catch {
    console.log('editor session_public_key invalid')
    await deleteDeviceKeys(editor_key_id.value)
    return await digestDeviceStorage()
  }
  try {
    // check for valid editor private key
    let privateKey = sessionStorage.getItem(
      keyStorage.getStorageId(editor_key_id.value)
    );
    if (privateKey)
      b64.ensureBinary(privateKey)
  }
  catch {
    console.log('editor session privateKey invalid')
    await deleteDeviceKeys(editor_key_id.value)
    return await digestDeviceStorage()
  }
  let has_session_key = Boolean(session_public_key && session_public_key.fingerprint);
  let has_key = has_local_key || has_session_key;

  /*
    form keys
  */
  let form_key_fingerprint
  let local_form_public_key;
  if (form_key_id.value && form_key_on_server.value) {
    try {
      local_form_public_key = await keyStorage.readLocked(`${form_key_id.value}_public`);
      b64.ensureBinary(local_form_public_key)
    }
    catch {
      console.log('form local_public_key invalid')
      await deleteDeviceKeys()
      return await digestDeviceStorage()
    }
  }
  try {
    // check for valid form private key
    let privateKey = sessionStorage.getItem(
      keyStorage.getStorageId(form_key_id.value)
    );
    if (privateKey)
      b64.ensureBinary(privateKey)
  }
  catch {
    console.log('form session privateKey invalid')
    await deleteDeviceKeys()
    return await digestDeviceStorage()
  }
  let local_ciphered_key;
  if (local_form_public_key) {
    local_ciphered_key = await keyStorage.readLocked(form_key_id.value);
  }
  let enabled_e2ee_form_user;
  if (public_key_on_server.value && form_key_on_server.value && e2ee_status.value.editors !== undefined) {
    enabled_e2ee_form_user = e2ee_status.value.editors.find(
      user => (
        user.e2ee_ciphered_key_backup.user_key_fingerprint == public_key_on_server.value.fingerprint &&
        user.e2ee_ciphered_key_backup.form_key_fingerprint == form_key_on_server.value.fingerprint
      )
    )
  }
  let remote_ciphered_key;
  if (enabled_e2ee_form_user) {
    remote_ciphered_key = enabled_e2ee_form_user.e2ee_ciphered_key_backup.ciphered_key
  }

  let local_user_fingerprint = function() {
    if (session_public_key)
      return session_public_key.fingerprint
    if (local_public_key)
      return local_public_key.fingerprint
  }();

  // Compare backup and fingerprints from server
  let user_key_remote_match =
    public_key_on_server.value &&
    local_user_fingerprint == public_key_on_server.value.fingerprint

  let ciphered_key_status = function() {
    if (!enabled_e2ee_form_user)
      return 'missing'
    if (!local_ciphered_key)
      return 'nolocal'
    else if (local_ciphered_key == remote_ciphered_key)
      return 'match'
    return 'invalid'
  }();
  if (ciphered_key_status == 'invalid') {
    console.log('ciphered_key_status == invalid')
    await deleteDeviceKeys()
    return await digestDeviceStorage()
  }

  let e2ee_state = function() {
    // Can be: locked, unlocked, need_passphrase and error
    if (has_session_key)
      return 'unlocked'
    if (has_local_key && !has_session_key)
      return 'need_passphrase'
    else if (has_local_key)
      return 'locked'
    return 'error'
  }();

  let would_lose_data = (function() {
    let status = e2ee_status.value
    let e2ee_status_forms = status.forms!==undefined ? status.forms : []
    return Boolean(
      //status.answer_count ||
      0 < e2ee_status_forms.map((x) => x.answer_count).reduce((x, y) => x + y, 0)
    )
  })();

  let has_LF_E2EE_Keys = await (async function() {
    for (let i = 0; i < localStorage.length; i++) {
      let key = await localStorage.key(i)
      if (key.startsWith(prefix)) {
        return true
      }
    }
    for (let i = 0; i < sessionStorage.length; i++) {
      let key = await sessionStorage.key(i)
      if (key.startsWith(prefix)) {
        return true
      }
    }
    return false
  })();

  let should_restore_key = function() {
    if (public_key_on_server.value) {
      if (!user_key_remote_match)
        return true
      if (!has_session_key)
        return true
    }
    return false
  }();

  let o = {
    has_local_key,
    has_key,
    would_lose_data,
    user_key_remote_match,
    should_restore_key,
    e2ee_state,
    has_LF_E2EE_Keys,
    lockedKey,
  }
  //console.log("digested keys",o)
  return o
}
