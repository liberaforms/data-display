/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

//import { createI18n } from "vue-i18n/dist/vue-i18n.runtime.esm-bundler.js";
import { createI18n } from "vue-i18n/dist/vue-i18n.esm-bundler.js";

function loadLocaleResources() {
  const locales = require.context(
    "@/translations",
    true,
    /[A-Za-z0-9-_,\s]+\.json$/i
  );
  const messages = {};
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);
    if (matched && matched.length > 1) {
      const locale = matched[1];
      messages[locale] = locales(key);
    }
  });
  return messages;
}

export default createI18n({
  legacy: false,
  globalInjection: true,
  //returnEmptyString: false,
  locale: "en",
  fallbackLocale: "en",
  messages: loadLocaleResources(),
});
