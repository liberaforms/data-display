/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { createApp } from 'vue'
import i18n from '@/i18n';
import { Vue3Mq } from "vue3-mq";
import PDFBuilder from "@/components/PDFBuilder.vue";

document.querySelectorAll("[vue-component=pdf-builder]")
        .forEach((element) => {
            createApp(PDFBuilder).use(i18n)
                                 .use(Vue3Mq, {
                                    // config options here
                                  })
                                 .mount(element)
});
