
//import * as L from 'leaflet';
import { icon } from 'leaflet';


const iconRetinaUrl = '/static/images/leaflet/marker-icon-2x.png';
const iconUrl = '/static/images/leaflet/marker-icon.png';
const shadowUrl = '/static/images/leaflet/marker-shadow.png';

export const iconDefault = icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
