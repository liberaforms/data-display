/*
This file is part of LiberaForms

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import { defineStore } from "pinia";

import axios from 'axios';
import { orderItems, isMultiChoice } from '@/modules/items.js'

import { parseISO, format } from 'date-fns'
import _ from 'underscore';
import { intersectionBy } from 'lodash';

import { useI18n } from "vue-i18n"
import i18n from '@/i18n.js';

import {keyStorage} from '@/modules/e2ee-answers.js'


async function decryptItems(formKeyId, userKeyId, orig_items, keyPassphrase) {
  //console.log("decryptItems formKeyId",formKeyId)
  //console.log("decryptItems userKeyId",userKeyId)
  //console.log("decryptItems orig_items",orig_items.length)
  //console.log("decryptItems keyPassphrase",keyPassphrase)
  const privKey = await keyStorage.readUnlocked(formKeyId, userKeyId, keyPassphrase);
  if (privKey === keyStorage.E_NEED_PASSPHRASE)
    return keyStorage.E_NEED_PASSPHRASE;
  const items = structuredClone(orig_items);
  for (var item of items)
  {
    const encData = item.data["e2ee-answer"];
    delete item.data["e2ee-answer"];
    const decData = await privKey.decrypt(encData);
    // item.data actually includes things useful for form attachments, so we merge
    item.data = {...JSON.parse(decData), ...item.data};
  }
  return items;
}

export const dataDisplayStore = defineStore("dataDisplayStore", {
  state: () => ({
    downloading_items: true,
    decrypting_items: false,
    ui_language: "en",
    endpoint: null,
    is_e2ee: false,
    download_response: null,
    e2ee_form_key_id: null,
    e2ee_editor_key_id: null,
    e2ee_state: 'unlocked', // Can be: locked, unlocked, need_passphrase and error
    e2ee_passphrase_attempt_count: 0,
    ui: {
      display_panel: true,
      numeric_totals: false,
      extended_filters: false,
      pdf_builder: false,
      other_options: false,
      delete_mode: false,
      slide_options: true,
    },
    item_endpoint: null,
    items: [],
    data_type: null,
    search_text: "",
    searched_items: [],
    deleted_fields: [],
    default_field_index: [],
    meta: {'ascending': true},
    user_prefs: {},
    page: 1,
    page_length: 10,
    card_batch: 1,
    card_batch_length: 20,
    can_edit: false,
    show_modes: false,
    has_row_controls: false,
    delete_item_initiated: null,  // is the id of the item
    include_deleted_fields: false,
    display_items_as: "grid",
    enabled_exports: [],
    extended_filters: {},
    pdf_prefs: {
      default: true,
      selected_fields: [],
      portrait: false,
      font_size: '8',
      page_size: 'A4',
    },
    edition_cnt: 0,
  }),
  getters: {
    itemsReadyToDisplay: (state) => {
      return !state.downloading_items && state.e2ee_state === "unlocked"
    },
    itemsToDisplay: (state) => {
      var items = state.searched_items.length || state.search_text ? state.searched_items : state.items
      let filtered_field_names = Object.keys(state.extended_filters)
      if (filtered_field_names.length == 0) return items

      //console.log("filtered_field_names", filtered_field_names)
      var _items = []
      Object.entries(items).forEach((item) => {
        //console.log("item",item[1])
        //console.log("answer keys", Object.keys(item[1].data))
        for (const [field_name, answer] of Object.entries(item[1].data)) {

          if (!filtered_field_names.includes(field_name)) continue
          let answered_options = answer.split(', ')
          let target_options = state.extended_filters[field_name]
          //console.log("answered_options",answered_options)
          //console.log("target_options",target_options)
          let inter = intersectionBy(answered_options, target_options)
          if (inter.length) {
            _items.push(item[1])
            break
          }
        }
      })
      return _items
    },
    paginatedItems: (state) => {
      let slice_start = (state.page - 1) * state.page_length
      let items_to_display = state.itemsToDisplay
      return items_to_display.slice(slice_start, slice_start + state.page_length)
    },
    paginatedCards: (state) => {
      let items_to_display = state.itemsToDisplay
      var slice_end = (state.card_batch + 1) * state.card_batch_length
      if (slice_end > items_to_display.length) {
        slice_end = items_to_display.length
      }
      return items_to_display.slice(0, slice_end)
    },
    first_field: (state) => {
      if (state.user_prefs.field_index[0].name != 'marked') {
        return state.user_prefs.field_index[0]
      } else {
        return state.user_prefs.field_index[1]
      }
    },
    getFormattedValue: (state) => (data) => {
      const { t: $t } = i18n.global

      var field_name = data.field_name
      var field_value = data.field_value
      if (field_value===undefined || field_value==="") {
        return ""
      }
      if (field_name == 'marked' || typeof field_value == "boolean") {
        return field_value ? $t("True") : $t("False")
      }
      if (field_name == 'created') {
        return format(parseISO(field_value), "yyyy-MM-dd kk:mm:ss")
      }
      if (isMultiChoice(field_name)){
        var result = []
        var option_values = field_value.split(', ')
        option_values.forEach((option_value) => {
          var label = state.getOptionLabel({'field_name': field_name,
                                            'option_value': option_value})
          result.push(label)
        })
        if (result.length > 0) {
          return result.join(', ')
        }
      }
      return field_value
    },
    getFieldLabel: (state) => (field_name) => {
      var field = state.field_index.find(x => x.name === field_name)
      if (field !== undefined) {
        return field.label
      }
      return null
    },
    all_multichoice_fields: (state) => {
      if (state.meta.form_structure===undefined) {
        return []
      }
      var result = []
      state.meta.form_structure.forEach((field) => {
        if (field.name && isMultiChoice(field.name)) {
          //let found = _.find(state.deleted_fields, function(f){ return f.name == field.name; })
          //console.log("deleted", field.name)
          result.push(field)
        }
      })
      return result
    },
    form_has_map: (state) => {
      if (state.meta.form_structure!==undefined) {
        for (let field of state.meta.form_structure) {
          if (field.name && field.name.startsWith("map-")) {
            return true
          }
        }
      }
      return false
    },
    all_map_fields: (state) => {
      var result = []
      if (state.meta.form_structure!==undefined) {
        state.meta.form_structure.forEach((field) => {
          if (field.name && field.name.startsWith("map-")) {
            result.push(field)
          }
        })
        if (state.include_deleted_fields) {
          state.deleted_fields.forEach((field) => {
            if (field.name && field.name.startsWith("map-")) {
              result.push(field)
            }
          })
        }
      }
      return result
    },
    getMapTiles: (state) => (field_name) => {
      for (let field of state.meta.form_structure) {
        if (field.name && field.name==field_name) {
          return field.tiles
        }
      }
      return 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    },
    getFieldOptions: (state) => (field_name) => {
      if (!isMultiChoice(field_name)) {
        return []
      }
      let field_structure = state.getFieldStructure(field_name)
      //console.log("getFieldOptions field_structure",field_structure)
      var _options = []
      for(let option in field_structure.values) {
        //console.log("getFieldOptions option",field_structure.values[option])
        _options.push(field_structure.values[option])
      }
      return _options
    },
    getOptionLabel: (state) => (payload) => {
      var field_name = payload.field_name
      var option_value = payload.option_value
      var field = state.getFieldStructure(field_name)
      var label = option_value
      if (field === undefined) {
        return label
      }
      field.values.forEach((value) => {
        if (value.value == option_value) {
          label = value.label
        }
      });
      return label
    },
    field_index: (state) => {
      if (state.include_deleted_fields && state.deleted_fields.length) {
        return state.user_prefs.field_index.concat(state.deleted_fields)
      }
      return state.user_prefs.field_index
    },
    all_field_structures: (state) => {
      return ! state.include_deleted_fields
             ? state.meta.form_structure
             : state.meta.form_structure.concat(state.deleted_fields)
    },
    getFieldStructure: (state) => (field_name) => {
      let structure
      structure = _.findWhere(state.meta.form_structure, {name: field_name})
      if (!structure && state.isFieldDeletedField(field_name)) {
        structure = _.findWhere(state.deleted_fields, {name: field_name})
        if (isMultiChoice(field_name) && structure.values===undefined) {
          // catch this for fields deleted prior to LiberaForms v3.3.0
          structure.values = structure.values ? structure.values!==undefined : []
        }
      }
      return structure
    },
    isFieldDeletedField: (state) => (field_name) => {
      return state.deleted_fields.find(x => x.name==field_name) != undefined
    },
    selected_answer_edition: (state) => {
      return state.items.find(
          edition => (edition.id == state.meta.selected_edition_id)
      )
    },
    previous_answer_edition: (state) => {
      let selected_pos = state.items.indexOf(state.selected_answer_edition)
      if (selected_pos < state.items.length -1) {
        return state.items[selected_pos + 1]
      }
      return null
    },
    failed_e2ee_decryption: (state) => {
      // likely because device_keys.ciphered_key_status == 'invalid'
      return (
        state.is_e2ee &&
        state.downloading_items == false &&
        state.download_response !== null &&
        state.download_response.data.items.length > state.items
      )
    },
  },
  actions: {
    setLanguage(lang_code) {
      const { locale } = useI18n({ useScope: 'global' })
      locale.value = lang_code
      this.ui_language = lang_code
    },
    async downloadItems() {
      await axios.get(this.endpoint).then(async response => {
        this.download_response = response
        // move everything except response.data.items
        // unpack meta
        this.meta = response.data.meta
        this.deleted_fields = this.meta.deleted_fields
        this.item_endpoint = this.meta.item_endpoint
        this.data_type = this.meta.data_type
        this.can_edit = this.meta.can_edit
        this.show_modes = this.meta.show_modes === true ? true : false
        this.enabled_exports = this.meta.enabled_exports !== undefined ? this.meta.enabled_exports : []
        this.default_field_index = this.meta.default_field_index
        // set user prefs
        this.user_prefs = response.data.user_prefs
        if (this.user_prefs.pdf !== null) {
          this.pdf_prefs = this.user_prefs.pdf
        }
        this.extended_filters = response.data.user_prefs.extended_filters!==undefined ? response.data.user_prefs.extended_filters : {}
        this.has_row_controls = this.data_type == 'answer' ? true : false

        if (this.is_e2ee && this.download_response.data.items.length) {
          await this.decryptItems()
        }
        else
          await this.finishLoadingItems(response.data.items)
        })
        .catch(e => {
          console.log(e)
        })
        .finally(() => {
          this.downloading_items = false
        });
    },
    async decryptItems(keyPassphrase) {
      //console.log("decryptItems this.download_response",this.download_response)
      //console.log("decryptItems this.e2ee_state",this.e2ee_state)
      //console.log("decryptItems keyPassphrase",keyPassphrase)
      this.decrypting_items = true
      this.e2ee_state = 'locked'
      try {
        const res = await decryptItems(this.e2ee_form_key_id,
                                       this.e2ee_editor_key_id,
                                       this.download_response.data.items,
                                       keyPassphrase)
        if (res === keyStorage.E_NEED_PASSPHRASE) {
          if (keyPassphrase) this.e2ee_passphrase_attempt_count += 1  // increments by 2 / when e2ee_form_key_id (I think)
          this.e2ee_state = 'need_passphrase'
        }
        else {
          this.e2ee_state = 'unlocked'
          this.decrypting_items = false
          await this.finishLoadingItems(res)
        }
      }
      catch (e) {
        this.e2ee_state = 'error'
      }
    },
    async finishLoadingItems(decrypted_items) {
        var items = orderItems(decrypted_items,
                               this.user_prefs.order_by,
                               this.user_prefs.ascending)
        this.items = items
        this.downloading_items = false
        this.download_response = null
    },
    setOrderBy(order_by) {
      this.user_prefs.order_by = order_by
      this.page = 1
      this.items = orderItems(this.items,
                              this.user_prefs.order_by,
                              this.user_prefs.ascending)
      axios.post(this.endpoint + '/order-by-field', {
        order_by_field_name: order_by
      })
      .then(function () {  // We could use the response argument
        //self.setOrderBy(response.data.order_by_field_name)
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    setAscending(ascending) {
      this.user_prefs.ascending = ascending
      this.page = 1
      this.items = orderItems(this.items,
                              this.user_prefs.order_by,
                              this.user_prefs.ascending)
      if (this.searched_items.length) {
        this.searched_items = orderItems(this.searched_items,
                                         this.user_prefs.order_by,
                                         this.user_prefs.ascending)
      }
      axios.post(this.endpoint + '/toggle-ascending')
      .then(function () {  // We could use the response argument
        //
      })
      .catch(function (error) {
        console.log(error);
      });
    },
  }
})
