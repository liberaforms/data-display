const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const webpack = require('webpack')

module.exports = {
  entry: {
      "data-display": {
        import: "./src/loaders/dataDisplayLoader.js",
      },
      "items-renderer": {
        import: "./src/loaders/itemsRendererLoader.js",
        dependOn: 'data-display',
      },
      "graph-renderer": {
        import: "./src/loaders/graphRendererLoader.js",
        dependOn: 'data-display',
      },
      "map-renderer": {
        import: "./src/loaders/mapRendererLoader.js",
        dependOn: 'data-display',
      },
      "pdf-builder": {
        import: "./src/loaders/pdfBuilderLoader.js",
        dependOn: 'data-display',
      },
      "answers-diff-renderer": {
        import: "./src/loaders/answersDiffLoader.js",
      },
      "e2ee-key-manager": {
        import: "./src/loaders/keyManagementLoader.js",
      },
      // We want to publish E2EE-code as a name-spaced library
      // That way we can easily share this code in multiple places
      "e2ee-answers": {
        import: "./src/modules/e2ee-answers.js",
        library: {
          name: "LF_E2EE",
          type: "var",
        },
      },
  },
  // We may run against a CORS issue when using the development server.
  // Setting the headers here helps against that
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
    },
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      //'vue$': 'vue/dist/vue.runtime.esm-bundler.js',
    },
    extensions: [
      '.js',
      '.json',
      '.vue',
    ],
  },
  output: {
    path: path.resolve(__dirname, './dist'),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
      },
      // this will apply to both plain `.js` files
      // AND `<script>` blocks in `.vue` files
      {
        test: /\.js$/,
        loader: "babel-loader",
      },
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: ["vue-style-loader", "css-loader"],
      },
      {
        test: /\.(json5?|ya?ml)$/, // target json, json5, yaml and yml files
        type: 'javascript/auto',
        loader: '@intlify/vue-i18n-loader',
        include: [ // Use `Rule.include` to specify the files of locale messages to be pre-compiled
          path.resolve(__dirname, './src/translations/**')
        ]
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
        "__VUE_OPTIONS_API__": true,
        "__VUE_PROD_DEVTOOLS__": false,
    })
    //new webpack.LoaderOptionsPlugin({
    //  minimize: true
    //})
  ]
}
